# -*- coding: utf-8 -*-
''' Faça um programa para escrever a contagem regressiva do lançamento 
de um foguete. O programa deve imprimir 10, 9, 8, 7 ... 1, 0 e Fogo! na tela.
'''

lanc = 11
while lanc > 0:
	lanc -= 1
	print lanc
	if lanc == 0:
		print "FOGO!"
