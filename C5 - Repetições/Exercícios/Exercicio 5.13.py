# -*- coding: utf-8 -*-
''' Escreva um programa que pergunte o valor inicial de uma dívida e o juro mensal. 
Pergunte também o valor mensal que será pago. Imprima o número de meses para que a dívida 
seja paga, o total pago e o total de juros pago. '''

divida = float(input("Valor da dívida: "))
juro_mensal = int(input("Juro mensal: "))
valor_mensal = float(input("Valor mensal que será pago: "))
divida = divida + (juro_mensal/100)
print ("Dívida total = %5.3f" % divida)
mes = 0
while divida > 0:
    divida = divida - valor_mensal
    mes += 1
    print ("\tMês %d\tFalta %5.3f" % (mes, divida))

print ("Serão necessários %d meses para que a dívida seja paga.\nTroco: %5.3f" % (mes,  (divida * -1)))
    
# divida total = divida * (juros / 100)
# rept : valor_mensal - divida
#       mês = mês + 1
