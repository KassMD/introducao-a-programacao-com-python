# -*- coding: utf-8 -*-
''' Escreva um programa para calcular a redução do tempo de vida de um fumante. 
Pergunte a quantidade de cigarros fumados por dia e por quantos anos ele já fumou.
Considere que um fumante perde 10 minutos de vida a cada cigarro, calcule 
quantos dias de vida um fumante perderá. Exiba o total em dias.'''

cigarros_por_dia = int(input("Quantos cigarros por dia: "))
anos_fumando = int(input("Quantos anos fumando: "))

minutos_perdido = (cigarros_por_dia * 10) * (anos_fumando * 365)
minutos_para_dias = (minutos_perdido / 24) / 60
print "Perderá %d dias de vida" % minutos_para_dias

# minutos_para_dias = (2880 / 24) / 60
