# -*- coding: utf-8 -*-
# Listagem 6.10 - Fatiamento de listas

L = [1, 2, 3, 4, 5]
print(L[0:5]) # Exibe do índice 0 ao 5
print(L[:5]) # Até o índice 5
print(L[:-1]) # Conta até o índice 4
print(L[:3]) # Até o índice 3
