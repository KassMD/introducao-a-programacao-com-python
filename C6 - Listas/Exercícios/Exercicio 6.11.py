# -*- coding: utf-8 -*-
''' Modifique o programa da listagem 6.15 usando for. Explique por que 
nem todos os while podem ser transformados em for. '''

def main():
	
	L = []
	
	while True:
		n = int(input("Digite um número (0 sai): "))
		if n == 0:
			break
		L.append(n)
	
	for x in L:
		print(x) 
	
	return 0

if __name__ == '__main__':
	main()

''' O primeiro while não pode ser transformado em for por que não sabemos 
quantas vezes ele ira se repetir. '''
