# -*- coding:utf-8 -*-
# Listagem 3.10 - Exemplo de composição com marcadores
'''
Imagine que precisamos apresentar um número como 001 ou 002, mas que também
pode ser algo como 050 ou 561. Nesse caso, estamos querendo apresentar um
número com três posições, completando com zeros à esquerda se o número for
menor.
Podemos realizar essa operação utilizando "%03d" % X. Observe que  adicionamos
03 entre  o % e o 'd'. Se você precisar apenas que o número ocupe três posições
, mas não desejar zeros à esquerda, basta retirar o zero e utilizar "%3d" % X.
Isso é muito importante quando estamos gravando dados em um arquivo ou
simplesmente exibindo informações na tela.
'''
idade = 22
print("[%d]" % idade)
print("[%03d]" % idade)
print("[%3d]" % idade)
print("[%-3d]" % idade)
