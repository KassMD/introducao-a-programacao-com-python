# -*- coding: utf-8 -*-
''' Modifique o programa anterior de forma que o usuário também digite 
o início e o fim da tabuada, em vez de começar com 1 e 10.'''

print ("\tTabuada de multiplicação")
inicio = int(input("Tabuada de: "))
fim = int(input("Até: "))
contador = 1
while  contador <= fim:
	print("%dx%d = %d" % (inicio, contador, (inicio * contador)))
	contador += 1
	
