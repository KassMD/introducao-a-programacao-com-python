# -*- coding: utf-8 -*-
''' Altere o programa anterior para exibir os resultados no mesmo formato 
de uma tabuada: 2x1 = 2, 2x4 = 4, ... '''

print "		Tabuada de multiplicação"
n = int(input("Tabuada de: "))
contador = 1
while contador <= 10:
	print("%dx%d = %d" % (n, contador, (n * contador)))
	contador = contador + 1 
