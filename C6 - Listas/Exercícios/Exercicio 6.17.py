#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 6.17
	Altere o programa da listagem 6.53 de forma a solicitar ao usuário o produto e a quantidade 
	vendida. Verifique se o nome do produto digitado existe no dicionário, e só então efetue 
	a baixa em estoque.
'''
def main():
	estoque = {	"tomate": [1000, 2.30],
						"alface": [500, 0.45],
						"batata": [2001, 1.20],
						"feijão": [100, 1.50]	}
	
	while True:
		print("\tCompra\n")
		produto = input("Digite o nome do produto [ \"exit\" para terminar]: ")
		
		if produto in estoque:
			quantidade = int(input("Digite a quantidade: "))
			print("> %s em estoque." % produto)
			estoque[produto][0] -= quantidade
			preço = estoque[produto][1] * quantidade
			print("\tPreço a pagar: %5.2f" % preço)
			
			print("--------------------------------------------")
			print("| Descrição: ", produto)
			print("| Quantidade: ", estoque[produto][0])
			print("| Preço: ", estoque[produto][1])
			print("--------------------------------------------")
		elif produto == 'exit':
			break
		else:
			print("> %s não existe no estoque." % produto)
		
	return 0
	
if __name__ == '__main__':
	main()
