#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.37 - Adivinhando o número
import random

def main():
	n = random.randint(1,10)
	x = int(input("Escolha um número entre 1 e 10: "))
	if x == n:
		print("Você acertou!")
	else:
		print("Você errou!")
	return 0

if __name__ == '__main__':
	main()

