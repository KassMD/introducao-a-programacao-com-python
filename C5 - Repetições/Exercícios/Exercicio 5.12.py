# -*- coding: utf-8 -*-
''' Altere o programa anterior de forma a perguntar também o valor depositado 
mensalmente. Esse valor será depositado no início de cada mês, e você deve 
considerá-lo para o cálculo de juros do mês seguinte. '''

contador = 1
total = 0

juros = float(input("Taxa de juros: "))
while contador <= 12:
	print ("\tMês %d" % contador)
	deposito = float(input("Deposito: "))
	total = total + ((deposito * juros) / 100)
	contador = contador + 1

print ("\nTotal de juros: %5.3f" % total)
