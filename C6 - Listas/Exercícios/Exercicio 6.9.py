# -*- coding: utf-8 -*-
''' Modifique o exemplo para pesquisar dois valores. Em vez de apenas p, leia 
outro valor v que também será procurado. Na impressão, indique qual dos valores 
foi achado primeiro. '''
# Pesquisa 2 valores
# Exibe qual foi achado primeiro
def main():
	lista = [1, 4, 13, 2, 5, 33, 19]
	contador = a = b = 0
	num_a = int(input("Digite o primeiro valor a ser procurado: "))
	num_b = int(input("Digite o segundo valor a ser procurado: "))
	
	while contador < len(lista):
		if num_a == lista[contador]:
			print("%d foi achado na posição %d" % (num_a, contador))
			a = contador
		elif num_b == lista[contador]:
			print("%d foi achado na posição %d" % (num_b, contador))
			b = contador
		contador += 1
		
	if a > b:
		print("\t%d foi encontrado primeiro." % num_b)
	else:
		print("\t%d foi encontrado primeiro." % num_a)
		
if __name__ == '__main__':
	main()
'''
if primeiro == False:
	primeiro == True
	p = num_a

if primeiro == True:
	print("%d foi o primeiro valor a ser achado." % p)
'''
