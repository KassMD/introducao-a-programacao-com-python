#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.18 - Função para imprimir uma barra na tela com parâmetros opcionais

def barra(n, caractere="*"):
	print(caractere * n)

def main():
	n = 10
	barra(n, "-")
	
	return 0

if __name__ == '__main__':
	main()
