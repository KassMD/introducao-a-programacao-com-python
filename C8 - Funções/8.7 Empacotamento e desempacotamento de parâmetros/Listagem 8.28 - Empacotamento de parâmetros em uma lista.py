#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.28 - Empacotamento de parâmetros em uma lista

def soma(a, b):
	print("%d + %d = %d" % (a, b, a + b))

def main():
	L = [2, 3]
	soma(*L)
	return 0

if __name__ == '__main__':
	main()

