#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.36 - Gerando números aleatórios 
import random

def main():
	for x in range(10):
		print(random.randint(1,100))
	return 0

if __name__ == '__main__':
	main()

