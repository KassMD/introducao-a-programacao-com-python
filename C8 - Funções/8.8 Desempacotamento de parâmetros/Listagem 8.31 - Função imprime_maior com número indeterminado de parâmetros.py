#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.31 - Função imprime_maior com número indeterminado de parâmetros

def imprime_maior(mensagem, *numeros):
	maior = None
	for e in numeros:
		if maior == None or maior <= e:
			maior = e
	print(mensagem, maior)

def main():
	imprime_maior("Maior:", 5,4,3,1)
	imprime_maior("Max:", *[1,7,9])
	
	return 0

if __name__ == '__main__':
	main()

