#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Modifique o programa anterior para também registrar a linha e a coluna de cada
ocorrência da palavra no arquivo. Para isso, utilize listas nos valores de cada
palavra, guardando a linha e a coluna de cada ocorrência.
- Registrar linha e coluna de cada palavra
- Salvar a linha e coluna em uma lista
'''
def main():
	lista_palavras = []
	palavra = ""
	dicionario = {}
	arquivo_texto = open("dicionario2.txt", "r")
	# lê cada letra, formando as palavras no final
	for cont_linha, linha in enumerate(arquivo_texto.readlines()):
		for coluna, letra in enumerate(linha):
			if letra.isalnum():
				palavra += letra
			else:
				lista_palavras.append(palavra.lower())
				print("A palavra |%s| está na linha %d, indo da coluna %d até a %d."
				% (palavra.center(12), cont_linha + 1, coluna - len(palavra), coluna))
				palavra = ""
	# correção das aspas duplas
	for indice, conteudo in enumerate(lista_palavras):
		if conteudo == "":
			del lista_palavras[indice]
	# conta quantas vezes cada palavra aparece, e dps adiciona ao dicionario
	for palavra in lista_palavras:
		qtda = lista_palavras.count(palavra)
		if palavra in dicionario:
			continue
		else:
			dicionario[palavra] = qtda
	# percorre o dicionario
	for chave, dados in dicionario.items():
		print("A palavra |%s| é citada %d vezes." % (chave.center(12), dados))

	arquivo_texto.close()
	return 0

if __name__ == '__main__':
	main()
