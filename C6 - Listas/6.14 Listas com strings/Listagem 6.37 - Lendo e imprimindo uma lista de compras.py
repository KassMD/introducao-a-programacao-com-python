#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 6.37 - Lendo e imprimindo uma lista de compras


def main():
	print("\tLista de compras")
	print("---------------------------------")
	Compras = []
	
	while True:
		produto = input("Insira um produto[0 para sair]: ")
		if produto == "0":
			break;
		Compras.append(produto)
		
	print("---------------------------------")
	for ind,elemen in enumerate(Compras):
		print("Produto %d - %s" % (ind + 1, elemen))
	return 0

if __name__ == '__main__':
	main()
