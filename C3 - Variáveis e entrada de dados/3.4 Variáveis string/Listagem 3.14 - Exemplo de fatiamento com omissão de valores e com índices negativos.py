# -*- coding:utf-8 -*-
# Listagem 3.14 - Exemplo de fatiamento com omissão de valores e com índices negativos
s = "ABCDEFGHI"
print(s[:2])
print(s[1:])
print(s[0:-2])
print(s[:])
print(s[-1:])
print(s[-5:7])
print(s[-2:-1])
