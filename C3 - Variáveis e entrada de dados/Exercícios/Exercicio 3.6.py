# -*- coding: utf-8 -*-
# Exercício 3.6
'''
Escreva uma expressão que será utilizada para decidir se um aluno foi ou não
aprovado. Para ser aprovado, todas as médias do aluno devem ser maiores que 7.
Considere que o aluno cursa apenas três matérias, e que a nota de cada uma
está armazenada nas seguintes variáveis: materia1, materia2 e materia3.
'''
nome = input("Digite seu nome: ")
mat1 = float(input("Digite a nota da primeira matéria: "))
mat2 = float(input("Digite a nota da segunda matéria: "))
mat3 = float(input("Digite a nota da terceira matéria: "))

if mat1 > 7 and mat2 > 7 and mat3 > 7:
	print("%s, você foi aprovado." % nome)
else:
	print("%s, você não foi aprovado." % nome)

