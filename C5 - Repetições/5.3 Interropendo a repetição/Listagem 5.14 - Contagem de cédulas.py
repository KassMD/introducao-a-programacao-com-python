# -*- coding: utf-8 -*-
# Listagem 5.14 - Contagem de cédulas
''' Programa que lê um valor e imprime a quantidade de cédulas
necessárias para pagar esse mesmo valor. '''

valor = int(input("Digite o valor a pagar: "))
cedulas = 0
atual = 50
apagar = valor
while True:
    if atual <= apagar: # 50 é menor ou igual a "apagar"?
        apagar -= atual # se verdadeiro: "apagar" - 50
        cedulas += 1 # cédulas + 1 até que a condição seja falsa
    else:
        print("%d cédula(s) de R$%d" % (cedulas, atual)) # total de cédulas contadas acima, mais atual
        if apagar == 0: # apagar == 0, o laço termina
            break
        if atual == 50: # se 50, vai para 20
            atual = 20
        elif atual == 20: # se 20, vai para 10
            atual = 10
        elif atual == 10: # se 10, vai para 5
            atual = 5
        elif atual == 5: # se 5, vai para 1
            atual = 1
        cedulas = 0
