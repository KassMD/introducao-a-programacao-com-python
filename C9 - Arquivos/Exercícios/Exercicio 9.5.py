#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 9.5
	Crie um programa que inverta a ordem das linhas do arquivo pares.txt
	A primeira linha deve conter o maior número; e a última, o menor.
'''
def Adlist(elemen):
	return int(elemen.strip())
	
def Revertlist(lista):
	lista.reverse()
	Gravarlist(lista)
	return lista

def Gravarlist(lista):
	pares = open("pares.txt", "w")
	for cont in lista:
		pares.write(str(cont))
		pares.write("\n")
	pares.close()

def main():
	lista = []
	pares = open("pares.txt")
	
	for line in pares.readlines():
		lista.append(Adlist(line))
	pares.close()
	
	lista = Revertlist(lista)
	print(lista)
	
	return 0

if __name__ == '__main__':
	main()

