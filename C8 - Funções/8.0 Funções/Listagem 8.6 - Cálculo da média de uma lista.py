#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.6 - Cálculo da média de uma lista
	
def soma(notas):
	total = 0
	for e in notas:
		total += e
	return total
	
def media(notas):
	return int( (soma(notas) / len(notas)) )
	
def main():
	notas = [5, 7, 7]
	print("Notas:", notas)
	print("Média:", media(notas))
	return 0	

if __name__ == '__main__':
	main()
