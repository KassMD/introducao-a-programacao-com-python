#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Crie um programa que leia um arquivo-texto e gere um arquivo de saída paginado.
Cada linha não deve conter mais de 76 caracteres e cada página terá no máximo 60 linhas.
Adicione na última linha de cada página o número da página atual e o nome do arquivo original.
'''
def main():
	arq_entrada = open("text_files/texto.txt", "r")
	arq_saida = open("text_files/textopaginado.txt", "w")
	cont_linha = 1
	cont_carac = 1
	pag = 1
	for linha in arq_entrada.readlines():
		for caractere in linha:
			arq_saida.write(caractere.strip("\n"))
			cont_carac += 1
			if cont_carac == 76:
				arq_saida.write("|¬\n")
				cont_carac = 0
				cont_linha += 1
				if cont_linha == 60:
					arq_saida.write("\n\n")
					arq_saida.write("\t\tPágina %d\tArquivo original: texto.txt" % pag)
					arq_saida.write("\n\n")
					pag += 1
					cont_linha = 0
					
	arq_saida.write("\n\n\t\tPágina %d\tArquivo original: texto.txt" % pag)
	arq_entrada.close()
	arq_saida.close()

if __name__ == '__main__':
	main()
