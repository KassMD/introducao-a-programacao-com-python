#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Modifique o programa para também receber o número de caracteres por linha e o
número de páginas por folha pela linha de comando.
'''
def main():
	arq_entrada = open("text_files/texto.txt", "r")
	arq_saida = open("text_files/textopaginado.txt", "w")
	cont_linha = 1
	cont_carac = 1
	total_linha = 1
	pag = 1
	
	for linha in arq_entrada.readlines():
		for caractere in linha:
			arq_saida.write(caractere.strip("\n"))
			cont_carac += 1
			if cont_carac == 76:
				arq_saida.write("|¬\n")
				print("Linha %d contém %d caracteres." % (total_linha, cont_carac))
				total_linha += 1
				cont_carac = 1
				cont_linha += 1
				if cont_linha == 60:
					arq_saida.write("\n\n")
					arq_saida.write("\t\tPágina %d\tArquivo original: texto.txt" % pag)
					arq_saida.write("\n\n")
					pag += 1
					cont_linha = 1
					
	arq_saida.write("\n\n\t\tPágina %d\tArquivo original: texto.txt" % pag)
	arq_entrada.close()
	arq_saida.close()

if __name__ == '__main__':
	main()
