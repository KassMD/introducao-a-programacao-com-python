# -*- coding: utf-8 -*-
# Listagem 6.26 - Pesquisa usando for

def main():
	L = [7,9,10,12]
	p = int(input("Digite um número a pesquisar: "))
	
	for e in L:
		if e == p:
			print("Elemento encontrado!")
			break
	else:
		print("Elemento não encontrado.")

if __name__ == '__main__':
	main()
