# -*- coding:utf-8 -*-
# Listagem 3.20 - Cálculo de bônus por tempo de serviço
''' A função input sempre retorna valores do tipo string, ou seja, não importa
se digitamos apenas números, o resultado sempre é string.
Para resolver esse pequeno problema, vamos utilizar a função int para
converter o valor retornando em um número inteiro, e a função float para
convertê-lo em número decimal ou de ponto flutuante.
Vejamos esse exemplo no qual devemos calcular o valor de um bônus por tempo
de serviço. '''

anos = int(input("Anos de serviço: "))
valor_por_ano = float(input("Valor por ano: "))
bonus = anos * valor_por_ano
print("Bônus de R$%5.2f" % bonus)
