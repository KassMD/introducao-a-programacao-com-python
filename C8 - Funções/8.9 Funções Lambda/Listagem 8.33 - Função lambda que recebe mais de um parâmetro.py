#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.33 - Função lambda que recebe mais de um parâmetro

def main():
	aumento = lambda a,b: (a*b/100)
	print(aumento(100, 5))
	return 0

if __name__ == '__main__':
	main()

