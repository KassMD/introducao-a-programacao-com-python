#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 9.1
	Escreva um programa que receba o nome de um arquivo pela linha de comando e que 
	imprima todas as linhas desse arquivo.
'''
def main():
	try:
		nomearq = input("Insira o nome do arquivo(com extensão): ")
		arquivo = open(nomearq, "r")
		for linha in arquivo.readlines():
			print(linha, end="")
		arquivo.close()
	except FileNotFoundError:
		print(" ERRO! Arquivo não encontrado.")
	return 0 

if __name__ == '__main__':
	main()

