# -*- coding: utf-8 -*-
''' Modifique o programa anterior(Exercicio 5.23) de forma a ler n números.
Imprima os n primeiros números primos. '''
# Números primos são os números naturais que têm apenas dois divisores diferentes: o 1 e ele mesmo
# 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59 - 101,103,107 - 839, 853, 857

limite = int(input("Digite um valor: "))
# SE LIMITE IGUAL A 2
if limite == 2:
	print("%d é um número primo." % limite)
	exit()
# SE LIMITE MENOR QUE 2
elif limite < 2:
	print("Insira um valor maior que 1.")
	exit()
# ATENDE AS CONDIÇÕES
else:
	num = 3
	resto = 0
	while num <= limite:
		if num % 2 != 0:
			cont = 3
			while cont < num:
				resto = num % cont
				if resto == 0:
					break
				cont += 1
			if cont == num:
				print("%d é um número primo." % num)
		num += 1	
