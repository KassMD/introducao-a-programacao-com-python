#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.8
	Usando a função MDC definida no exercício anterior, defina uma função para calcular 
	o menor múltiplo comum (M.M.C) entre dois números.
'''

def MDC(a, b):
	if a != 0: 
		a, b = b % a, a
		return MDC(a, b)
	else:
		return b
		
def MMC(a,b, mdc):
	mmc = int((a * b) / mdc)
	return mmc
	
def main():
	a = 6
	b = 4
	
	mdc = MDC(a,b)
	print("MMC:", MMC(a,b, mdc))
	
	return 0
	
if __name__ == '__main__':
	main()
