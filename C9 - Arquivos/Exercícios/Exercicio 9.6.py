#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
- Imprimir 40 vezes o símbolo "=" se este for o primeiro caractere
- Adicionar a opção para parar de imprimir ao se pressionar Enter cada vez que uma
  linha iniciar com "."
'''
def main():
	largura = 79
	entrada = open("text_files/entrada.txt")
	for linha in entrada.readlines():
		if linha[0] == ";":
			continue
		elif linha[0] == ">":
			print(linha[1:].rjust(largura))
		elif linha[0] == "=":
			for cont in range(40):
				print(linha[1:])
		#elif linha[0] == ".":
		elif linha[0] == "*":
			print(linha[1:].center(largura + 5))
		else:
			print(linha)
	return 0

if __name__ == '__main__':
	main()
