#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.29 - Outro exemplo de empacotamento de parâmetros em uma lista

def barra(n = 10, c = "*"):
	print(c*n)

def soma(a, b):
	print("%d + %d = %d" % (a, b, a + b))

def main():
	print("Com caracteres:")
	L = [ [5, "-"], [ 10, "*"], [5], [6, "."] ]
	for e in L:
		barra(*e)
	print("Com números:")
	L = [ [5, 11], [ 10, 23], [5, 8], [6, 14] ]
	for e in L:
		soma(*e)
	return 0

if __name__ == '__main__':
	main()

