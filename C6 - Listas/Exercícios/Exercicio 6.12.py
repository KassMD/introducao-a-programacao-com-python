# -*- coding: utf-8 -*-
''' Altere o programa da listagem 6.33 de forma a imprimir o menor elemento da 
lista. '''

def main():
	L = [1,7,2,4]
	maximo = L[0]
	menor = L[0]
	for e in L:
		if e > maximo:
			maximo = e
		elif e < menor:
			menor = e
	print("maior valor: %d" % maximo)
	print("menor valor: %d" % menor)
	return 0

if __name__ == '__main__':
	main()
