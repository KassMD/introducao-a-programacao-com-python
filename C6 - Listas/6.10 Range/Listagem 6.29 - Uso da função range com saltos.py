# -*- coding: utf-8 -*-
# Listagem 6.29 - Uso da função range com saltos

def main():
	
	for t in range(2,100,2):
		print(t, end=" ")
	
	return 0

if __name__ == '__main__':
	main()

''' Observe que um gerador como o retornado pela função range não é exatamente 
uma lista. Embora seja usado de forma parecida, é na realidade um objeto de 
outro tipo. Para transformar um gerador em lista, utilize a função list '''
