#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.10
	Reescreva a função para cálculo da sequência de Fibonacci, sem utilizar recursão.
'''

def Fibonacci(sequencia):
	n = 1
	while n < 600:
		ultimo = sequencia[-2] + sequencia[-1]
		sequencia.append(ultimo)
		print(sequencia)
		n += 1
	return sequencia

def main():
	lista = [0,1]
	lista = Fibonacci(lista)
	return 0

if __name__ == '__main__':
	main()
