# -*- coding: utf-8 -*-
# Listagem 4.6 - Conta de telefone com três faixas de preço

minutos = int(input("Quantos minutos você utilizou este mês? "))
if minutos < 200:
	preco = 0.20
else:
	if minutos > 200 and minutos < 400:
		preco = 0.15

print "Você vai pagar %d este mês." % (minutos * preco)
