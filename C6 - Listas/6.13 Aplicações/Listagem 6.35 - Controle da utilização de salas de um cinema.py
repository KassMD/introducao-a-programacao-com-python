# -*- coding: utf-8 -*-
#  Listagem 2.35 - Controle da utilização de salas de um cinema
"""
Vejamos um programa que controla a utilização das salas de um cinema.
Imagine que a lista Salas = [10, 2, 1, 3, 0] contenha o número de lugares vagos 
nas salas 1, 2, 3, 4 e 5, respectivamente. Esse programa lerá o número da sala 
e a quantidade de lugares solicitados, ou seja, se ainda há um lugares livres. 
Caso seja possível vender os bilhetes atualizará o número de lugares livres. 
A saída ocorre quando se digita 0 no número da Sala.
"""

def main():
	
	lugares_vagos = [10, 2, 1, 0]
	print("Existem %d salas." % len(lugares_vagos))
	
	while True:
		sala = int(input("Escolha uma sala [0 sai]: "))
		# Verifica se a sala é igual a 0
		if sala == 0:
			print("Bye..") 
			break
		# Verifica se o número escolhido é maior que o número de salas disponiveis
		# ou menor que 1
		if sala > len(lugares_vagos) or sala < 1: 
			print("Sala inválida")
		# Verifica se a sala está lotada
		elif lugares_vagos[sala - 1] == 0:
			print("Desculpe, sala lotada!")
		else:
			lugares = int(input("Quantos lugares você deseja, existe %d vagos: " % lugares_vagos[sala - 1]))
			if lugares > lugares_vagos[sala - 1]:
				print("Esse número de lugares não está disponível.")
			elif lugares < 0:
				print("Número inválido.")
			else:
				lugares_vagos[sala - 1] -= lugares
				print("%d lugares vendidos" % lugares)
	
	print("Utilização das salas")
	for x,l in enumerate(lugares_vagos):
		print("Sala %d - %d lugar(es) vazio(s)" % (x + 1, l))
	
	return 0

if __name__ == '__main__':
	main()
