# -*- coding: utf-8 -*-
''' Escreva um programa que exiba uma lista de opções (menus): adição, subtração, 
divisão, multiplicação e sair: Imprima a tabuada da operação escolhida.
Repita até que a opção saída seja escolhida. '''

while True:
	escolha = input("Tabuada com as 4 operações matemáticas\n\tA - Adição\n\tS - Subtração\n\tD - Divisão\n\tM - Multiplicação\n\t0 - Sair\n\t\t: ")
	#  SAIR
	if escolha == '0':
		exit()
	#  ADIÇÃO
	elif escolha == 'A' or escolha == 'a':
		limite = int(input("Exibir até qual tabuada? "))
		tabuada = 1
		while tabuada <= limite:
			numero = 0
			while numero < 11:
				print(" %d + %d = %d" % (tabuada, numero, tabuada + numero))
				numero +=1
			tabuada +=1
			print("\n")
	#  SUBTRAÇÃO
	elif escolha == 'S' or escolha == 's':
		limite = int(input("Exibir até qual tabuada? "))
		tabuada = 1
		while tabuada <= limite:
			numero = 0
			while numero < 11:
				print("%d - %d = %d" % (tabuada, numero, tabuada - numero))
				numero += 1
			tabuada += 1
			print("\n")
	#  DIVISÃO
	elif escolha == 'D' or escolha == 'd':
		limite = int(input("Exibir até qual tabuada? "))
		tabuada = 1
		while tabuada <= limite:
			numero = 1
			while numero < 11:
				print(" %d / %d = %d |r = %d" % (tabuada, numero, tabuada/numero, tabuada % numero))
				numero += 1
			tabuada += 1
			print("\n")
	#  MULTIPLICAÇÃO
	elif escolha == 'M' or escolha == 'm':
		limite = int(input("Exibir até qual tabuada? "))
		tabuada = 1
		while tabuada <= limite:
			numero = 0
			while numero < 11:
				print(" %d x %d = %d" % (tabuada, numero, tabuada * numero))
				numero += 1
			tabuada += 1
			print("\n")
