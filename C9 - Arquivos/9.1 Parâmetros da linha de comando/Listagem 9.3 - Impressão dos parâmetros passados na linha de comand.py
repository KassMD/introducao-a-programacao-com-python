#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 9.3 - Impressão dos parâmetros passados na linha de comando
import sys

def main():
	print("Primeiro Segundo Terceiro")
	print("1 2 3")
	print("Número de parâmetros: %d" % len(sys.argv))
	for n,p in enumerate(sys.argv):
		print("Parâmetro %d = %s" % (n,p))
	return 0

if __name__ == '__main__':
	main()

