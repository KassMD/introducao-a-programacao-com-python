#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.9 - Cálculo do fatorial
	
def fatorial(n):
	fat = 1
	while n > 1:
		fat *= n
		n -= 1
	return fat

def main():
	n = int(input("Digite um valor: "))
	print("Fatorial de %d é %d." % (n, fatorial(n)))

if __name__ == '__main__':
	main()
