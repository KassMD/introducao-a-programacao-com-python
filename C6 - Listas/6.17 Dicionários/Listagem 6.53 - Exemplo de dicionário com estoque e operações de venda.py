#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 6.53 - Exemplo de dicionário com estoque e operações de venda

def main():
	estoque = {	"tomate": [1000, 2.30],
						"alface": [500, 0.45],
						"batata": [2001, 1.20],
						"feijão": [100, 1.50]	}
	venda = [ ["tomate", 5], ["batata", 10], ["alface", 5] ]
	total = 0
	print("\tVendas:\n")
	for operação in venda:
		produto, quantidade = operação
		preço = estoque[produto][1]
		custo = preço * quantidade
		print("%s: %3d x %6.2f = %6.2f" % (produto, quantidade, preço, custo))
		estoque[produto][0] -= quantidade
		total += custo
	
	print("Custo total: %10.2f\n" % total)
	print("\tEstoque:\n")
	
	for chave, dados in estoque.items():
		print("Descrição: ", chave)
		print("Quantidade: ", dados[0])
		print("Preço: ", dados[1])
		print()
	
	return 0
	
if __name__ == '__main__':
	main()

'''
O método "items" retorna uma tupla contendo a chave e o valor  de cada item armazenado 
no dicionário.
'''
