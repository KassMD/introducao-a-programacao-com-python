# -*- coding:utf-8 -*-
# Listagem 3.9 - Exemplo de concatenação
'''
O conteúdo de variáveis string podem ser somados, ou melhor, concatenados.
Para concatenar duas strings, utilizamos o operador de adição (+).
Assim "AB" + "C" é igual a "ABC".
'''
s = "ABC"
print(s + "C")
print(s + "D" * 4)
print("X" + "-" * 10 + "X")
print(s + "x4 = " + s * 4)