# -*- coding: utf-8 -*-
''' Escreva um programa que leia dois números. Imprima o resultado da multiplicação 
do primeiro pelo segundo. Utilize apenas os operadores de soma e subtração para 
calcular o resultado. '''

n1 = int(input("Digite o primeiro número: "))
n2 = int(input("Digite o segundo número: "))
copia = n1
cont = n2

while cont > 1 :
	copia = copia + n1
	cont = cont - 1
	
print "%dx%d = %d" % (n1, n2, copia) 
