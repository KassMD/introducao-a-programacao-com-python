#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 9.4
	Crie um programa que receba o nome de dois arquivos como parâmetros da linha de 
	comando e que gere um arquivo de saída com as linhas do primeiro e do segundo arquivo.
'''
def main():
	arquivo = input("Digite o nome do arquivo(com extensão): ")
	arquivo2 = input("Digite o nome do arquivo(com extensão): ")
	arquivomisto = open("text_files/arquivomisto.txt", "w")
	
	try:
		with open(arquivo) as arquivo_obj:
			a = arquivo_obj.read()
			arquivomisto.write(a)
	except FileNotFoundError:
		print(" ERRO! %s não foi encontrado." % arquivo)		
	arquivomisto.write("\n")
	try:
		with open(arquivo2) as arquivo2_obj:
			b = arquivo2_obj.read()
			arquivomisto.write(b)
	except FileNotFoundError:
		print(" ERRO! %s não foi encontrado." % arquivo2)
	
	arquivomisto.close()
	return 0
	

if __name__ == '__main__':
	main()

