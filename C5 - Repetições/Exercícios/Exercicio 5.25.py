# -*- coding: utf8 -*-
''' Escreva um programa que calcule a raiz quadrada de um número. Utilize o método 
de Newton para obter um resultado aproximado. Sendo 'n' o número a obter a raiz 
quadrada, considere a base 'b = 2'. Calcule 'p' usando a fórmula 'p=(b+(n/b))/2'.
Agora calcule o quadrado de p. A cada passo, faça 'b=p' e recalcule 'p' usando a 
fórmula apresentada. Pare quando a diferença absoluta entre 'n' e o quadradode 'p' 
for menor que 0,0001. '''
# Anotações
# 1. Peça um número 'n', o qual obter a raiz
# 2. Base 'b = 2'
# 3. Calcule 'p' usando a fórmula p=(b+(n/b))/2
# 4. Calcule o quadrado de 'p'
# 5. A cada passo faça 'b = p' e recalcule 'p' usando a fórmula
# 6. Parar quando a diferença entre 'n' e o quadrado de 'p' for menor que 0.0001


n = int(input("Digite um número: "))
base = 2
while True:
	p = (base + (n / base)) / 2
	p = p**base
	base = p
	if ((n - p) < 0.0001):
		break
		
print("Raiz quadrada de %d: %5.4f" % (n, p))
