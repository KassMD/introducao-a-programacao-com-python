#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.2 - Definição do retorno de um valor
'''

'''
def main():
	print("Soma: ",soma(2,5))
	
	return 0

def soma(a,b):
	return(a + b)

if __name__ == '__main__':
	main()
