# -*- coding: utf-8 -*-
# Listagem 6.13 - Repetição com tamanho da lista usando len

L = [1, 2, 3, 4, 5, 6, 7]
x = 0
while x < len(L):
    print(L[x])
    x += 1
