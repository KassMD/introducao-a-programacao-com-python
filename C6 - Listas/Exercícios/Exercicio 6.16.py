#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 6.16
Modifique o programa da listagem 6.44 para ordenar a lista em ordem descrecente: 
L = [1,2,3,4,5] deve ser ordenada como L = [5,4,3,2,1]
'''
import random

def main():
	Lista = []
	
	for cont in range(5):
		r = random.randint(1, 11)
		Lista.append(r)
	fim = len(Lista)
	
	print("Lista desordenada: \t", Lista)
	print("---------------------------------------------")
	while fim > 1:
		trocou = False
		x = 0
		while x < (fim - 1):
			if Lista[x] < Lista[x + 1]:
				trocou = True
				temp = Lista[x]
				Lista[x] = Lista[x + 1]
				Lista[x + 1] = temp
			x += 1
			print("Ordenando lista: \t", Lista, "\t x vale %d\t Trocou = %s" % (x, trocou))
		print("=============================================")
		if not trocou:
			break
		fim -= 1
	
	print("---------------------------------------------")
	print("Lista ordenada: \t", Lista)

	return 0

if __name__ == '__main__':
	main()

