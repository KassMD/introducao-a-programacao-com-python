# -*- coding: utf-8
# Listagem 6.31 - Impressão de índices SEM usar a função enumarate

def main():
	L = [5,9,13]
	x = 0
	for e in L:
		print("[%d] %d" % (x,e))
		x += 1
		
	return 0

if __name__ == '__main__':
	main()
