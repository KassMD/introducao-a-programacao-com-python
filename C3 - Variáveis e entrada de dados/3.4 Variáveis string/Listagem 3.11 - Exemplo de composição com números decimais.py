# -*- coding:utf-8 -*-
# Listagem 3.11 - Exemplo de composição com números decimais

print("%5f" % 5)
print("%5.2f" % 5)
print("%10.5f" % 5)
