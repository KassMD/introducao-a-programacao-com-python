# -*- coding: utf-8 -*-
''' Escreva um programa que calcule o preço a pagar pelo fornecimento de 
energia elétrica. Pergunte a quantidade de KWh consumida e o tipo de 
instalação: R para residências, I para industrias, C para comércios.
Calcule o preço a pagar de acordo com a tabela.'''

kwh = int(input("Quantidade de KWh consumidas: "))
tipo_inst = input("Tipo de instalação:\n\tr = residencial  c = comercial  i = industrial\n\t\t: ")

if tipo_inst == 'R' or tipo_inst == 'r':
    if kwh <= 500:
        preco = kwh * 0.40
    else:
        preco = kwh * 0.65
    tipo_inst = "Residencial"

if tipo_inst == 'C' or tipo_inst == 'c':
    if kwh <= 1000:
        preco = kwh * 0.55
    else:
        preco = kwh * 0.60
    tipo_inst = "Comercial"

if tipo_inst == 'I' or tipo_inst == 'i':
    if kwh <= 5000:
        preco = kwh * 0.55
    else:
        preco = kwh * 0.60
    tipo_inst = "Industrial"

print ("\tInstalação e preço a pagar:\n\t\tInstalação: %s\tPreço: %d" % (tipo_inst, preco))
