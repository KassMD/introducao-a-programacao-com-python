#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.11
	Escreva uma função para validar uma variável string. Essa função recebe como parâmetro 
	a string, o número mínimo e máximo de caracteres. Retorne verdadeiro se o tamanho 
	da string estiver entre os valores de máximo e mínimo, e falso em caso contrário.
'''
def main():
	frase = input("Digite uma frase com no máximo 20c, e no mínimo 10: ")
	maximo = 20
	minimo = 10
	print(validacao(frase, maximo, minimo))
	return 0

def validacao(frase, maximo, minimo):
	return len(frase) > 10 and len(frase) < 20
	
if __name__ == '__main__':
	main()
