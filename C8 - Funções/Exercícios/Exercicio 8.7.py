#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.7
	Defina uma função recursiva que calcule o maior divisor comum(M.D.C) entre dois números 
	'a' e 'b', onde a > b.
'''
def MDC(divisor, dividendo):
	if divisor != 0: # 5 repetição == False
		temp = divisor
		#30		1rep
		#18		2rep
		#12		3rep
		#6		4rep
		divisor = dividendo % divisor
		#18		1rep
		#12		2rep
		#6		3rep
		#0		4rep
		dividendo = temp
		#48 -> 30		1rep
		#30 -> 18		2rep
		#18 -> 12		3rep
		#12 -> 6		4rep
		return MDC(divisor, dividendo)
		#1rep		#18		,		30
		#2rep		#12		,		18
		#3rep		#6		,		12
		#4rep		#0		,		6
	else:
		return dividendo

def main():
	a = 48
	b = 30

	print("MDC:", MDC(a, b))
	
	return 0
	
if __name__ == '__main__':
	main()

#print("%d / %d = %d | resto %d" % (dividendo, divisor, dividendo / divisor, dividendo % divisor))
#dividendo, divisor = divisor, dividendo % divisor
