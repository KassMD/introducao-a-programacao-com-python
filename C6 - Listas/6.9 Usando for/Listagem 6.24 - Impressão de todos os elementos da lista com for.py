# -*- coding: utf-8 -*-
# Listagem 6.24 - Impressão de todos os elementos da lista com for
def main():
	
	e = 0
	Lista = [8,9,15]
	
	for e in Lista:
		print(e)
	
	return 0

if __name__ == '__main__':
	main()
