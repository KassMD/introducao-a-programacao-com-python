# -*- coding:utf-8 -*-
# Listagem 3.4 - Operador not

not True
not False

verdade = True
falso = False

if verdade == True:
	print(not verdade)
	
if falso == False:
	print(not falso)
