# -*- coding: utf-8 -*-
# Listagem 6.33 - Verificação do maior valor (Utilizando números random)
import random

def main():
	
	L = []
	for g in range(10):
		a =	random.randrange(1, 21)
		L.append(a)
	print(L)
	maximo = L[0]
	for e in L:
		if e > maximo:
			maximo = e
	print("maior valor: %d" % maximo)
	return 0

if __name__ == '__main__':
	main()
