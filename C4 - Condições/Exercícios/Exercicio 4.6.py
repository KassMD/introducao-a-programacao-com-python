# -*- coding: utf-8 -*-
''' Escreva um programa que pergunte a distância que um passageiro deseja 
percorrer em km. Calcule o preço da passagem, cobrando R$0,50 por km para 
viagens de até 200km, e R$0,45 para viagens mais longas. '''

km = int(input("Km percorridos: "))
if km <= 200 :
	passagem = km * 0.50
else:
	passagem = km * 0.45
print "Preço da passagem é %d " % passagem
