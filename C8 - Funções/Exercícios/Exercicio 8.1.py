#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random

''' Exercicio 8.1
	Escreva uma função que retorne o maior de dois números.
	Valores esperados:
	maximo(5,6) == 6
	maximo(2,1) == 2
	maximo(7,7) == 7
'''
def main():
	a = random.randint(1, 9)
	b = random.randint(1, 9)
	print("a = %d\tb = %d" % (a, b))
	print("Maior valor: %d" % (MaiorValor(a, b)))
		
	return 0

def MaiorValor(v1, v2):
	if v1 > v2:
		return v1
	elif v2 > v1:
		return v2
	else:
		return v1
	
	
if __name__ == '__main__':
	main()

