# -*- coding: utf-8 -*-
# Listagem 5.16 - Impressão de tabuadas sem repetições aninhadas

limite = int(input("Digite a tabuada: "))
tabuada = 1
numero = 1
while True:
	print(" %d x %d = %d" % (tabuada, numero, tabuada * numero))
	numero += 1
	if numero == 11:
		tabuada += 1
		numero = 1
		print("\n")
		if tabuada > limite:
			break
