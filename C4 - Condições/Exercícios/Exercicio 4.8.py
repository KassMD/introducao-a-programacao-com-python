# -*- coding: utf-8 -*-
''' Escreva um programa que leia dois números e que pergunte qual operação você 
deseja realizar. Você deve poder calcular a soma(+), subtração(-), multiplicação(*) 
e divisão(/). Exiba o resultado da operação solicitada. '''

print "		Operações matématicas\n"
num1 = int(input("Digite o primeiro número: "))
num2 = int(input("Digite o segundo número: "))
print ""
operacao = int(input("Digite a operação que deseja fazer:\n   	1 - Soma\n	2 - Subtração\n	3 - Multiplicação\n	4 - Divisão\n		: "))

if operacao == 1:
	result = num1 + num2
elif operacao == 2:
	result = num1 - num2
elif operacao == 3:
	result = num1 * num2
elif operacao == 4:
	result = num1 / num2

print "\nResultado: %d" % result
