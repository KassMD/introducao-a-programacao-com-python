# -*- coding: utf-8 -*-
''' Escreva um programa que leia um número e verifique se é ou não é um número 
primo. Para fazer essa verificação, calcule o resto da divisão do número por 2 
e depois por todos os números ímpares até o número lido. Se o resto de uma dessas 
divisões for igual a ZERO, o número não é primo. Observe que 0 e 1 não são primos 
e que 2 é o único número primo que é par. '''
# Números primos são os números naturais que têm apenas dois divisores diferentes: o 1 e ele mesmo

num = int(input("Digite um número: "))
#  Se 2
if num == 2:
	print("%d é um número primo." % num)
#  Se divisor de 2 ou menor que 2
elif num % 2 == 0 or num < 2:
	print("%d não é um número primo." % num)
#  Se não divisor de 2
else:
	cont = 1
	while cont < num:
		resto = cont % num
		if resto == 0:
			print("%d não é primo" % num)
			break
		cont += 1
	print("%d é um número primo." % num)
