#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.12 - Função modificada para facilitar o rastreamento

def fatorial(n):
	print("Calculando o fatorial de %d" % n)
	if n == 0 or n == 1:
		print("Fatorial de %d = 1" % n)
		return 1
	else:
		fat = n * fatorial(n - 1)
		print("Fatorial de %d = %d" % (n, fat))
		
	return fat
		

def main():
	n = int(input("Digite um valor: "))
	n, fatorial(n)

if __name__ == '__main__':
	main()
