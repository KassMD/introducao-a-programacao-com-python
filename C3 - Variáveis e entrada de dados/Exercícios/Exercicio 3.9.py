# -*- coding: utf-8 -*-
''' Escreva um programa que leia a quantidade de dias, horas, minutos e segundos
do usuário. Calcule o total em segundos. '''

# Ler os dados
print("    Insira os dados")
dias = int(input("Dias: "))
horas = int(input("Horas: "))
minutos = int(input("Minutos: "))
segundos = int(input("Segundos: "))

# Apresentar os dados
print("    Dados lidos")
print("Dias = %d\nHoras = %d\nMinutos = %d\nSegundos = %d" % (dias, horas, minutos,
segundos))
# Conversão dos dados
dias_para_horas = dias * 24
horas_para_minutos = horas * 60
minutos_para_segundos = minutos * 60

# Apresentar dados convertidos
print("    Dados convertidos")
print("Dias -> horas = %d\nHoras -> minutos = %d\nMinutos -> segundos = %d\nSegundos = %d" % (dias_para_horas,
horas_para_minutos, minutos_para_segundos, segundos))

# Calcular total em segundos
dias_para_horas = (dias * 24) + horas
horas_para_minutos = (dias_para_horas * 60) + minutos
minutos_para_segundos = (horas_para_minutos * 60) + segundos
calculo_geral = minutos_para_segundos

# Apresentar total
print("    Total em segundos")
print("Total = %d" % calculo_geral)
