#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 6.50 - Observação do preço com um dicionário

def main():
	tabela = {	"Alface": 0.45,
					"Batata": 1.20,
					"Tomate": 2.30,
					"Feijão": 1.50	}
	while True:
		produto = input("Digite o nome do produto [ \"exit\" para terminar]: ")
		if produto == "exit":
			break
		elif produto in tabela: #1
			print("Preço %5.2f" % tabela[produto]) #2
		else:
			print("Produto não encontrado.")
	
	return 0

if __name__ == '__main__':
	main()

'''
Em #1 verificamos se o dicionário contém a chave procurada. Em caso afirmativo, 
imprimimos o preço associano à chave, ou haverá uma mensagem de erro. 
'''
