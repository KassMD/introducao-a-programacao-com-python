#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 9.2 - Abrindo, lendo e fechando um arquivo
def main():
	arquivo = open("números.txt", "r")
	for linha in arquivo.readlines():
		print(linha, end="")
	arquivo.close()
	
	return 0

if __name__ == '__main__':
	main()

'''
	O método readlines gera uma lista em que cada elemento é uma linha do arquivo. 
	Na linha 7 é impresso a linha na tela.
'''
