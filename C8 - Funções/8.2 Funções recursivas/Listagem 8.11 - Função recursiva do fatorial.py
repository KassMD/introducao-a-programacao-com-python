#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.11 - Função recursiva do fatorial

def fatorial(n):
	if n == 0 or n == 1:
		return 1
	else:
		return n * fatorial(n - 1)

def main():
	n = int(input("Digite um valor: "))
	print("Fatorial de %d é %d." % (n, fatorial(n)))

if __name__ == '__main__':
	main()
