# -*- coding: utf-8 -*-
''' Escreva um programa que pergunte o depósito inicial e a taxa de juros 
de uma poupança. Exiba os valores mês a mês para os 24 primeiros meses.
Escreva o total ganho com juros no período.'''

deposito_inicial = float(input("Deposito inicial: "))
juros = float(input("Taxa de juros: "))
contador = 0
total = 0

# (deposito_inicial x juros) / 100
while contador < 24:
	total = total + ((deposito_inicial * juros) / 100)
	contador = contador + 1

print ("Total de juros: %5.3f" % total)
	
