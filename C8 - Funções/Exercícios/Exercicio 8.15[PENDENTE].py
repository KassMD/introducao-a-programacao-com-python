#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.15
	Utilizando a função type, escreva uma função recursiva que imprima os elementos de uma lista.
	Cada elemento deve ser impresso separadamente, um por linha. Considere o caso de listas 
	dentro de listas, como L = [1, [2, 3, 4, [5,6,7]]].
	A cada nível, imprima a lista mais à direita, como fazemos ao identar blocos em Python.
	Dica: Envie o nível atual como parâmetro e utilize-o para calcular a quantidade de espaços 
	em branco à esquerda de cada elemento.
'''
def main():

	return 0
	
if __name__ == '__main__':
	main()
