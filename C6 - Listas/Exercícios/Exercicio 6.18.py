#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 6.18
	Escreva um programa que gere um dicionário, onde cada chave seja um caractere, e seu 
	valor seja o número desse caractere encontrado em uma frase lida.
	Exemplo: O rato -> {"O": 1, "r":2, "a":3, "t": 4, "o": 5}
'''
def main():
	dicionario = {}
	frase = input("Insira uma frase: ")
	contador = 0
	for letra in frase:
		contador += 1
		dicionario[letra] = contador
	
	for letra, num in dicionario.items():
		print(letra, num,"\t", end="")
	
	
	
if __name__ == '__main__':
	main()
