#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.35 - Módulo soma (soma.py) que importa entrada.py  
import entrada

def main():
	Lista = []
	for x in range(10):
		Lista.append(entrada.valida_inteiro("Digite um número: ", 0, 20))
	print("Soma:", sum(Lista))
	return 0

if __name__ == '__main__':
	main()

