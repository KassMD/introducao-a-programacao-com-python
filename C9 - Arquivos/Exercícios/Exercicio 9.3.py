#!/usr/bin/env python3
# -*- coding: utf-8 -*-
''' Exercicio 9.3
	Crie um programa que leia os arquivos pares.txt e impares.txt e que crie um só arquivo 
	pareseimpares.txt com todas as linhas dos outros dois arquivos, de forma a preservar a 
	ordem numérica.
'''

def main():
	impares = open("impares.txt", "r")
	pares = open("pares.txt", "r")
	pares_impares = open("pares_impares.txt", "w")
	lista = []

	for linha in impares.readlines():
		lista.append(int(linha))
	for linha in pares.readlines():
		lista.append(int(linha))

	lista.sort()
	for linha in lista:
		pares_impares.write("%d\n" % linha)

	return 0

if __name__ == '__main__':
    main()
