# -*- coding: utf-8 -*-
''' Faça um programa que Calcule o aumento de um salário. Ele deve solicitar o valor
do salário e a porcentagem do aumento. Exiba o valor do aumento e do novo salário. '''

print("  Aumento salarial")
salario_inicial = float(input("Insira o valor do salário: "))
porcentagem = float(input("Insira a porcentagem do aumento: "))

porcentagem = porcentagem / 100
aumento = salario_inicial * porcentagem
salario_final = salario_inicial + aumento
print("Porcentagem = %5.2f" % porcentagem)
print("Aumento = R$%5.3f" % aumento)
print("Salário final = R$%5.3f" % salario_final)
