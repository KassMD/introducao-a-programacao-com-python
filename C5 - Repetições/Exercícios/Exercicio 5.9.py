# -*- coding: utf-8 -*-
''' Escreva um programa que leia dois números. Imprima a divisão inteira do primeiro 
pelo segundo, assim como o resto da divisão. Utilize apenas os operadores de soma 
e subtração para calcular o resultado.
Lembre-se de que podemos entender o quociente da divisão de dois números como 
a quantidade de vezes que podemos retirar o divisor do dividendo. Logo, 20 / 4 = 5,
uma vez que podemos subtrair 4 cinco vezes de 20.'''

n1 = int(input("Digite o primeiro número: "))
n2 = int(input("Digite o segundo número: "))
copia = n1
x = 0
while copia >= n2 : # Enquanto 20 for maior ou igual a 4, ele continuará executando
	copia = copia - n2 # 20 - 4 a cada execução
	x = x + 1 # Conta quantas vezes 4 foi tirado de 20

print ("")
print ("%d/%d = %d" % (n1, n2, x))
print ("resto = %d" % copia)
