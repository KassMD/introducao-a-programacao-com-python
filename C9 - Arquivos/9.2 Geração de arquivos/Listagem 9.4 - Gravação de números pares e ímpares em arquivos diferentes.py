#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Listagem 9.4 - Gravação de números pares e ímpares em arquivos diferentes
def main(args):
	impares = open('impares.txt', 'w')
	pares = open('pares.txt', 'w')
	for num in range(0, 1001):
		if num % 2 == 0:
			pares.write('%d\n' % num)
		else:
			impares.write('%d\n' % num)
	impares.close()
	pares.close()
	
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
