# -*- coding: utf-8 -*-
a = 1 # a recebe 1
b = 5 # b recebe 5
c = 2 # c recebe 2
d = 1 # d recebe 1

print(a == b) # a é igual a b?
print(b > a) # b é maior que a?
print(a < b) # a é menor que b?
print(a == d) # a é igual a d?
print(b >= a) # b é maior ou igual a a?
print(c <= b) # c é menor ou igual a b?
print(d != a) # d é diferente de a?
print(d != b) # d é diferente de b?
