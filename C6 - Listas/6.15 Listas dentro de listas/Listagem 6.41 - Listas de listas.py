#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 6.41 - Listas de listas

def main():
	
	Produto1 = ["maça", 10, 0.30]
	Produto2 = ["pera", 5, 0.75]
	Produto3 = ["kiwi", 4, 0.98]
	
	Compras = [Produto1, Produto2, Produto3]
	
	cont = 0
	for prod in Compras:
		cont += 1
		print("Produto %d: %s" % (cont, prod))
	
	return 0

if __name__ == '__main__':
	main()
