#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.13
	Altere o programa da listagem 8.37 de forma que o usuário tenha três chances de acertar 
	o número. O programa termina se o usuário acertar ou errar três vezes.
'''
import random

def main():
	n = random.randint(1,10)
	chances = 3
	while chances > 0:
		x = int(input("Escolha um número entre 1 e 10: "))
		if x == n:
			print("Você acertou!")
			chances = 0
		else:
			print("Você errou!")
			chances -= 1
	return 0
	
if __name__ == '__main__':
	main()
