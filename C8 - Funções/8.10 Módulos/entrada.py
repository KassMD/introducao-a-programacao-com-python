#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.34 - Módulo entrada

def valida_inteiro(mensagem, minimo, maximo):
	while True:
		try:
			v = int(input(mensagem))
			if v >= minimo and v <= maximo:
				return v
			else:
				print("Digite um valor entre %d e %d " % (minimo,maximo))
		except:
			print("Você deve digitar um número inteiro.")
	
	return 0
