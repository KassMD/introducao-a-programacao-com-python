# -*- coding: utf-8 -*-
''' Escreva um programa para aprovar o empréstimo bancário para compra de uma casa.
O programa deve perguntar o valor da casa a comprar, o salário e a quantidade de anos 
a pagar. O valor da prestação mensal não pode ser superior a 30% do salário.
Calcule o valor da prestação como sendo o valor da casa a comprar dividido pelo número 
de meses a pagar.'''

valor_casa = float(input("Valor da casa: "))
salario = float(input("Salário: "))
qtda_anos = int(input("Quantidade de anos a pagar: "))

qtda_meses = qtda_anos * 12 #Converte a quantidade de anos para meses
prestacao = valor_casa / qtda_meses
if  prestacao > (salario * 30/100):
    print ("\tO valor da prestação não pode ser superior a 30% do salário.")
else:
    print("Aprovado.\n\tValor da prestação: %5.3f" % prestacao)
