# -*- coding: utf-8 -*-
''' Escreva um programa que pergunte a quantidade de km percorridos por um carro 
alugado pelo usuário, assim como a quantidade de dias pelos quais o carro foi alugado.
Calcule o preço a pagar, sabendo que o carro custa R$60 por dia e 
R$0,15 por km rodado. '''

km_percorridos = float(input("Km percorridos: "))
qtd_dias = int(input("Quantidade de dias: "))

preco_pagar = (60 * qtd_dias) + (0.15 * km_percorridos)
print "Preço a pagar = %.2f" % preco_pagar
