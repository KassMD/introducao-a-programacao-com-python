#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.4
	Escreva uma função que receba a base e a altura de um triângulo e  retorne sua área 
	(Area = base(base x altura) / 2)
	area_triangulo(6,9) == 27
	area_triangulo(5,8) == 20
'''
def main():
	b = int(input("Digite a base do triângulo: "))
	a = int(input("Digite a altura do triângulo: "))
	
	print("A área do triângulo é", AreaTriangulo(b, a))
	
	return 0
	
def AreaTriangulo(base, altura):
	area = int((base * altura) / 2)
	return area
	
if __name__ == '__main__':
	main()
