#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 9.6 - Processamento de um arquivo

def main():
	largura = 79
	entrada = open("text_files/entrada.txt")
	for linha in entrada.readlines():
		if linha[0] == ";":
			continue
		elif linha[0] == ">":
			print(linha[1:].rjust(largura))
		elif linha[0] == "*":
			print(linha[1:].center(largura + 5))
		else:
			print(linha)
	return 0

if __name__ == '__main__':
	main()
