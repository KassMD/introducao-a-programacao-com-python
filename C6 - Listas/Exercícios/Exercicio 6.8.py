# -*- coding: utf-8 -*-
''' Modifique o primeiro exemplo(Listagem 6.23) de forma a realizar a 
mesma tarefa, mas sem utilizar a variável "achou". Dica: observe a condição de 
saída do while. '''

def main():
	cont = 0
	lista = [1, 4, 15, 7, 27, 39]
	num = int(input("Digite o número a procurar: "))
	while cont < len(lista):
		if num == lista[cont]:
			print("%d achado na posição %d" % (num, cont))
			break
		cont += 1
	if cont == len(lista):
		print("%d não foi achado." % num)
		
if __name__ == '__main__':
	main()
	
