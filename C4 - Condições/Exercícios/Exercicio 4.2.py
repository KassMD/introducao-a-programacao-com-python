# -*- coding: utf-8 -*-
''' Escreva um programa que pergunte a velocidade do carro de um usuário.
Caso ultrapasse 80km/h, exiba uma mensagem dizendo que o usuário foi multado.
Nesse caso, exiba o valor da multa, cobrando R$5 por km acima de 80km/h. '''

velocidade = int(input("Digite a velocidade do carro: "))
if velocidade > 80:
	multa = (velocidade - 80) * 5
	print "Valor da multa é %d" % multa
