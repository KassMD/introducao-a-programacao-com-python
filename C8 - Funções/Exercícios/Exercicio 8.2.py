#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random

''' Exercicio 8.2
	Escreva uma função que recebe dois números e retorna 'True' se o primeiro valor for 
	múltiplo do segundo.
'''
def main():
	a = random.randint(1,9)
	b = random.randint(1,9)
	print(a, b)
	print(Multiplos(a,b))
	
	return 0

def Multiplos(x, y):
	return x%y == 0
	
if __name__ == '__main__':
	main()
