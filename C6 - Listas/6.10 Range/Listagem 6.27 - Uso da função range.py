# -*- coding: utf-8 -*-
# Listagem 6.27 - Uso da função range

def main():
	
	for v in range(10):
		print(v)
	
	return 0

if __name__ == '__main__':
	main()

''' A função range gerou números de 0 a 9 porque passamos 10 como parâmetro. 
Ela normalmente gera valores a partir de 0, logo, ao especificarmos apenas 10, 
estamos apenas informando onde parar. '''
