# -*- coding: utf-8 -*-
# Listagem 6.12 - Repetição com tamanho fixo

L = [1, 2, 3]
x = 0
while x < 3:
    print(L[x])
    x += 1
