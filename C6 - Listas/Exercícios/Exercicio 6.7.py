#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''	Exercício 6.7
	Faça um programa que leia uma expressão com parênteses. Usando pilhas, verifique se os parênteses 
	foram abertos e fechados na ordem correta.
'''

def main():
	Parenteses = [ ]
	print("\tVerificação de parênteses")
	#		ENTRADA
	incluir = input("Digite uma sequência de parênteses: ")
	
	#		PROCESSO
		# inclui os caracteres na lista
	for cont in incluir:
		Parenteses.append(cont)		
		# Conta os caracteres da lista
	while len(Parenteses) != 0:
		if Parenteses[0] == '(' and len(Parenteses) > 1: # Se o primeiro elemento for (
			del Parenteses[0] # Deleta o elemento do indice 0
			for i, e in enumerate(Parenteses):
				if e == ')': # Se achar )
					del Parenteses[i] # Deleta o elemento do indice
					break # Sai do for
			else:
				print("Sequência incorreta.")
				exit
		else:
			print("Sequência incorreta.")
			break
				
	#		SAÍDA
	if len(Parenteses) == 0:
		print("Sequência correta.")

	return 0

if __name__ == '__main__':
	main()

