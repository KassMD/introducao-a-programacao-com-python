# -*- coding: utf-8 -*-
''' Escreva um programa que leia números inteiros do teclado. O programa deve ler os números até
que o usuário digite 0(zero). No final da execução exiba a quantidade de numeros digitados, assim
como a soma e a média aritmética. '''

print("\t\tMédia aritmética")
soma = 0
qtda = 0
while True:
    num = int(input("Digite o número: "))
    if num == 0:
        break
    soma += num
    qtda += 1
print("Qtda de números digitados = %d\nMédia = %d" % ( qtda, (soma / qtda)))
