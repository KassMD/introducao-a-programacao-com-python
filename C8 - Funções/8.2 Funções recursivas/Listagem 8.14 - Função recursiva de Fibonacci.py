#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.14 - Função recursiva de Fibonacci
import random
def Fibonacci(n):
	if n <= 1:
		return n
	else:
		return Fibonacci(n - 1) + Fibonacci(n - 2)

def main():
	n = random.randint(2,9)
	print("N:", n)
	print(Fibonacci(n))
	return 0

if __name__ == '__main__':
	main()

