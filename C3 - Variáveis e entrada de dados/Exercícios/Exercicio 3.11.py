# -*- coding: utf-8 -*-
''' Faça um programa que solicite o preço de uma mercadoria e o percentual de desconto.
Exiba o valor do desconto e o preço a pagar. '''

preco_mercadoria = float(input("Insira o preço da mercadoria: "))
percentual_desconto = float(input("Digite o percentual de desconto: "))

percentual_desconto = percentual_desconto / 100
desconto = preco_mercadoria * percentual_desconto
preco_pagar = preco_mercadoria - desconto

print "Valor do desconto na mercadoria = R$%5.2f\nValor a pagar = %5.2f" % (desconto, preco_pagar)
