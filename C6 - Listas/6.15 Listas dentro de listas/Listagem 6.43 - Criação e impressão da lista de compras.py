#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 6.43 - Criação e impressão da lista de compras

def main():
	
	Compras = []
	
	while True:
		produto = input("Produto: ")
		if produto == "0":
			break
		quantidade = int(input("Quantidade: "))
		preço = float(input("Preço: "))
		Compras.append([produto, quantidade, preço])
		print("------------------")
	soma = 0.0
	
	for e in Compras:
		print("%20s x%5d %5.2f %6.2f" % (e[0], e[1], e[2], e[1] * e[2]))
		soma += e[1] * e[2]
		
	print("Total: %7.2f" % soma)
	
	return 0

if __name__ == '__main__':
	main()

