#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.32 - Função lambda que recebe um valor e retorna o dobro dele 

def main():
	a = lambda x: x * 2
	print(a(3))
	
	return 0

if __name__ == '__main__':
	main()

