# -*- coding: utf-8 -*-
# Listagem 6.28 - Uso da função range com intervalos

def main():
	
	for v in range(5,13 + 1):
		print(v)
	
	return 0

if __name__ == '__main__':
	main()

