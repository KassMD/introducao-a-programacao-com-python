#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.13
	Altere o programa da listagem 8.37 de forma que o usuário tenha três chances de acertar 
	o número. O programa termina se o usuário acertar ou errar três vezes.
'''
import random

def Verifica(esc, num, chances):
	if Valida(esc):
		if esc == num:
			print("Você acertou!")
			chances = 0
			return chances
		else:
			print("Você errou!")
			chances -= 1
			return chances
	else:
		chances -= 1
		return chances

def Valida(esc):
	if esc >= 1 and esc <= 10:
		return True
	else:
		print("Digite um número inteiro entre 1 e 10.")
		return False

def main():
	chances = 3
	n = random.randint(1,10)
	while chances > 0:
		escolha = int(input("Escolha um número de 1 a 10: "))
		chances = Verifica(escolha, n, chances)
	
	return 0
	
if __name__ == '__main__':
	main()
