# -*- coding: utf-8 -*-
''' Altere o programa da listagem 6.21 de forma a poder trabalhar com vários 
comandos digitados de uma só vez. Atualmente apenas um comando pode ser inserido 
por vez. Altere-o de forma a considerar operação como uma string.

Exemplo: FFFAAAS, significa três chegadas de novos clientes, três atendimentos 
e finalmente a saída do programa.
'''
def main():
	ultimo = 5
	fila = list(range(1, ultimo + 1))
	print("> Fila atual: ", fila)
	while True:
		# --- ENTRADA --- #
		print("F para adicionar cliente. A para realizar atendimento. ", end="")
		print("S para sair")
		inclusao = input("Operação(F, A ou S): ")
		print("----------------------------------")
		# --- PROCESSO --- #
		contador = 0
		while contador < len(inclusao):
			operacao = inclusao[contador]
			if operacao == "A" or operacao == "a":
				if len(fila) > 0:
					atendido = fila.pop(0)
					print("Cliente %d atendido" % atendido)
				else:
					print("Fila vazia!")
			elif operacao == "F" or operacao == "f":
				#ultimo += 1
				ultimo = fila[-1] + 1
				fila.append(ultimo)
				print("Cliente %d entrou na fila." % ultimo)
			elif operacao == "S" or operacao == "s":
				print("Fila atual: ", fila)
				exit()
			else:
				print("Operação inválida!")
			contador += 1
			
		# --- CONCLUSÃO --- #
		print("Fila atual: ", fila)
		print("----------------------------------")
	return 0
	
if __name__ == '__main__':
	main()
