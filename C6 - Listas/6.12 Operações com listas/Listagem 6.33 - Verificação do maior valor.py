# -*- coding: utf-8 -*-
# Listagem 6.33 - Verificação do maior valor

def main():
	L = [1,7,2,4]
	maximo = L[0]
	for e in L:
		if e > maximo:
			maximo = e
	print("maior valor: %d" % maximo)
	return 0

if __name__ == '__main__':
	main()
