#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.30 - Função soma com número indeterminado de parâmetros

def soma(*args):
	s = 0
	for x in args:
		s += x
	return s

def main():
	print(soma(1,2))
	print(soma(2))
	print(soma(5,6,7,8))
	print(soma(9,10,20,30,40))
	
	return 0

if __name__ == '__main__':
	main()

