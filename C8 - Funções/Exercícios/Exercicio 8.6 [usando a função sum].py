#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.6
	Reescreva o programa da listagem 8.8 de forma a utilizar for em vez de while.
'''
def soma(lista):
	tam = sum(lista)
	return tam

def main():
	Lista = [5, 2, 5]
	print("Lista: ", Lista)
	print("Soma:", soma(Lista))
	return 0
	
	
if __name__ == '__main__':
	main()
