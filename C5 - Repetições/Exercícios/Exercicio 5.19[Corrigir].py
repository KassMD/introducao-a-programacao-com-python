# -*- coding: utf-8 -*-

valor = float(input("Digite um valor: "))
apagar = valor
atual = 100
cedulas = 0
while True:
    if atual <= apagar: # 100 é menor ou igual a à pagar? #SIM
        apagar -= atual # Tirar 'atual' de 'à pagar'
        cedulas += 1 # 1+ cédula
    else:
        print("%d cédula/moeda de R$%3.2f" % (cedulas, atual)) # Exibe cédulas contadas no if acima
        if apagar == 0 or apagar < 0.01: # Se apagar == 0, o programa termina
            break
        # Cédulas
        elif atual == 100: # Atual igual a 100
            atual = 50 # Recebe 50
        elif atual == 50:
            atual = 20
        elif atual == 20:
            atual =  10
        elif atual == 10:
            atual = 5
        elif atual == 5:
            atual = 1
                
        # Moedas
        elif atual == 1:
            atual = 00.50
        elif atual == 00.50:
            atual = 00.10
        elif atual == 00.10:
            atual = 00.05
        elif atual == 00.05:
            atual = 00.01

        cedulas = 0 # ZERA cédulas para retomar a contagem no próximo if
