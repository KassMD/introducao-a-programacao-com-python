#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 6.44 - Ordenação pelo Método Bolha [Bubble Sort]
import random
def main():
	Lista = []
	fim = 5
	
	for cont in range(fim):
		r = random.randint(1, 26)
		Lista.append(r)
	
	print("Lista desordenada: \t", Lista)
	print("--------------------------------------------------")
	while fim > 1:
		trocou = False
		x = 0
		while x < (fim - 1):
			if Lista[x] > Lista[x + 1]:
				trocou = True
				Lista[x], Lista[x + 1] = Lista[x + 1], Lista[x]
			x += 1
			print("Ordenando Lista: \t", Lista, "\t x vale %d\t Trocou =  %s" % (x, trocou))
		print("+   +   +   +   +   +   +   +   +   +   +   +   +   +")
		if not trocou:
			break
		fim -= 1
	print("--------------------------------------------------")
	print("Lista ordenada: \t", Lista)
	
	return 0

if __name__ == '__main__':
	main()
