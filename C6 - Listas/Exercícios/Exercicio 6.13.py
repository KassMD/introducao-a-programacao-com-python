#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' A lista de temperaturas de Mons, na Bélgica, foi armazenada na lista 
T = [-10,-8,0,1,2,5,-2,-4]. Faça um programa que imprimira a menor e a maior 
temperatura, assim como a temperatura média. '''

def main():
	Temp_Mons = [-10,-8,0,1,2,5,-2,-4]
	maxima = Temp_Mons[0]
	minima = Temp_Mons[0]
	media = 0
	
	for count in Temp_Mons:
		if count > maxima:
			maxima = count
		else:
			if count < minima:
				minima = count
		media += count
	print("Highest temperature: %d" % maxima)
	print("Average temperature: %d" % (media/len(Temp_Mons)))
	print("Lowest temperature: %d" % minima)
	return 0

if __name__ == '__main__':
	main()

