#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 6.39 - Impressão de uma lista de strings, letra a letra


def main():
	Lista = ["maças", "peras", "kiwis"]
	
	for s in Lista:
		print("")
		for letra in s:
			print(letra)
	return 0

if __name__ == '__main__':
	main()
