#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.1 - Definição de uma nova função
'''
	A definição da função prepara o interpretador para executar a função quando esta for 
	chamada em outras partes do programa. Para chamar uma função definida pelo 
	programa, faça da mesma forma que as funções já definidas na linguagem, ou seja, 
	nome da função seguido dos parâmetros entre parenteses.
'''
def main():
	soma(2, 9)
	soma(7, 8)
	soma(10, 15)
	
	return 0

def soma(a,b):
	print(a + b)

if __name__ == '__main__':
	main()

