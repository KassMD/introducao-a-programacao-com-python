# -*- coding: utf-8 -*-
# Listagem 6.34 - Coṕia de elementos para outras listas

def main():
	V = [9,8,7,12,0,13,21]
	P = []
	I = []
	for e in V: # e percorre V
		if e % 2 == 0: # Se e for par...
			P.append(e) # Elemento par é adicionado a P
		else: # Se não
			I.append(e) # Elemento impar é adicionado a I 
	print("Pares", P)
	print("Impares:", I)
	
	return 0

if __name__ == '__main__':
	main()

