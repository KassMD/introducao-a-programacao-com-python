# -*- coding: utf-8 -*-
''' Reescreva o programa da Listagem 5.14 de forma a continuar executando até que 
o valor digitado seja 0. Utilize repetições aninhadas. '''

while True:
	valor = int(input("Digite o valor a pagar, ou 0 para sair: "))
	if valor == 0:
		print("\tSaindo...")
		break
	cedulas = 0
	atual = 100
	a_pagar = valor
	while True:
		if atual <= a_pagar:
			a_pagar = a_pagar - atual
			cedulas += 1
		else:
			print("%d cédulas de R$%d" % (cedulas, atual))
			if a_pagar == 0:
				break
			elif atual == 100:
				atual = 50
			elif atual == 50:
				atual = 20
			elif atual == 20:
				atual = 10
			elif atual == 10:
				atual = 5
			elif atual == 5:
				atual = 1
			cedulas = 0
