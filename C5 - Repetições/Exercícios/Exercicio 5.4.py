# -*- coding: utf-8 -*-
''' Modifique o programa anterior para imprimir de 1 até o número 
digitado pelo usuário, mas, desta vez, apenas os números ímpares. '''

x = int(input("Digite um número superior a 10: "))
y = 1
while y <= x:
	y += 1
	if y % 2 != 0:
		print y
