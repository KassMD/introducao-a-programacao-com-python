# -*- coding: utf-8 -*-
''' Escreva um programa que converta uma temperatura digitada em ºC em ºF.
A fórmula para essa conversão é: F = (( 9 x C ) / 5) + 32 '''

C = int(input("Digite a temperatura em ºC: "))
F = (( 9 * C ) / 5) +  32
print "Em ºF = %d" % F
