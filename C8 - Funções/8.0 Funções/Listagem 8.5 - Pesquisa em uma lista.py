#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.5 - Pesquisa em uma lista
import random

def main():
	lista = []
	for e in range(10):
		x = random.randint(10, 50)
		lista.append(x)
	print("Lista:", lista)
	
	checar = int(input("A posição de qual número você quer checar? "))
	print("Esse número pertence ao índice", Pesquisa(lista, checar)) 
	
	return 0
	
def Pesquisa(lista, checar):
	for indice, valor in enumerate(lista):
		if valor == checar:
			return indice

if __name__ == '__main__':
	main()

