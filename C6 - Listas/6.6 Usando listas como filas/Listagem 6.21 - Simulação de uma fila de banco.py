# -*- coding: utf-8 -*-
# Listagem 6.21 - Simulação de uma fila de banco 
def main():
	print("\t\tSimulação de uma fila de banco")
	ultimo = 10
	fila = list(range(1, ultimo + 1))
	while True:
		print("\nExistem %d clientes na fila" % len(fila))
		print("Fila atual: ", fila)
		print("Digite F para adicionar um cliente ao fim na fila,")
		print("ou A para realizar o atendimento. S para sair.")
		operacao = input("Operação(F, A ou S): ")
		
		if operacao == "A" or operacao == "a":
			if(len(fila)) > 0:
				atendido = fila.pop(0) 
				# .pop retorna o valor do elemento e o exclui da fila.
				print("Cliente %d atendido" % atendido)
			else:
				print("Fila vazia! Ninguém para atender.")
		elif operacao == "F" or operacao == "f":
			ultimo += 1 # Incrementa o ticket do novo cliente
			fila.append(ultimo)
		elif operacao == "S" or operacao == "s":
			break
		else:
			print("Operação inválida! Digite apenas F, A ou S!")
	
	return 0
	
if __name__ == '__main__':
	main()
