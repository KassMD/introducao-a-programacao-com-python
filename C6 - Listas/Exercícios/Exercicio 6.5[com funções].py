#!/usr/bin/env python
# -*- coding: utf-8 -*-
def main():
	repetir = True
	fila = list(range(1, 6))
	print("Fila:", fila)
	print("F para adicionar cliente. A para realizar atendimento. ", end="")
	print("E para exibir fila, S para sair")
	
	while repetir:
		incluir = ManipularFila()
		for letra in incluir:
			if letra.upper() == "A":
				fila = AtendeCliente(fila)
			elif letra.upper() == "F":
				fila = NovoCliente(fila)
			elif letra.upper() == "E":
				ExibirFila(fila)
			elif letra.upper() == "S":
				repetir = False
			else:
				print("> Operação %s inválida! <" % letra)
				
	return 0
	
def ManipularFila(op = ""):
	op = input("Operação(F, A, E ou S): ")
	print("--------------------------------------------------------")
	return op.strip()

def AtendeCliente(fila):
	if len(fila) > 0:
		atendido = fila.pop(0)
		print("Cliente %d atendido." % atendido)
	else:
		print("Fila vazia!")
	return fila

def NovoCliente(fila):
	if len(fila) > 0:
		ultimo = fila[-1] + 1
		fila.append(ultimo)
		print("Cliente %d entrou na fila." % ultimo)
	else:
		fila.append(1)
	return fila
	
def ExibirFila(fila):
	print("\t\tFila", fila, "\n")

if __name__ == '__main__':
	main()
