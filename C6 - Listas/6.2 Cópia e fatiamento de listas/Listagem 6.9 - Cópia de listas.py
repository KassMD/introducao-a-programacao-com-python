# -*- coding: utf-8 -*-
# Listagem 6.9 - Cópia de listas

L = [1, 2, 3, 4, 5]
V = L[:] # Copia lista L para V
print(L)
print(V)
V[0] = 6 # Alterna o índice 0 de V
print(L)
print(V)
