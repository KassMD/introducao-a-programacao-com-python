#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
def main():
	fila = list(range(1,6))
	while len(fila) > 0:
		print("Fila: ", fila)
		
		x = random.randint(1,4)
		if x == 1:
			fila = AtendeCliente(fila)
		elif x == 2 or x == 3:
			fila = NovoCliente(fila)
		
	return 0

def AtendeCliente(fila):
	atendido = fila.pop(0)
	return fila

def NovoCliente(fila):
	ultimo = fila[-1] + 1
	fila.append(ultimo)
	return fila

if __name__ == '__main__':
	main()
