#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 9.1 Abrindo, escrevendo e fechando um arquivo

def main():
	arquivo = open("números.txt", "w")
	for linha in range(1, 101):
		arquivo.write("%d\n" % linha)
	arquivo.close()
	return 0

if __name__ == '__main__':
	main()

