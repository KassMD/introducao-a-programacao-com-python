#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.27 - Configuração de funções com funções

def imprime_lista(L, impressao, condicao):
	for e in L:
		if condicao(e):
			impressao(e)

def imprime_elemento(e):
	print("Valor: %d" % e)

def epar(x):
	return x % 2 == 0

def eimpar(x):
	return not epar(x)

def main():
	
	Lista = list(range(1,11))
	imprime_lista(Lista, imprime_elemento, epar)
	imprime_lista(Lista, imprime_elemento, eimpar)
	return 0

if __name__ == '__main__':
	main()

