#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Crie um programa que leia um arquivo e crie um dicionário onde cada chave é uma
palavra e cada valor é o número de ocorrências no arquivo.
'''
def main():
	lista_palavras = []
	palavra = ""
	dicionario = {}
	arquivo_texto = open("dicionario.txt", "r")
	# lê cada letra, formando as palavras no final
	for linha in arquivo_texto.readlines():
		for letra in linha:
			if letra.isalnum():
				palavra += letra
			else:
				lista_palavras.append(palavra.lower())
				palavra = ""
	# correção das aspas duplas
	for indice, conteudo in enumerate(lista_palavras):
		if conteudo == "":
			del lista_palavras[indice]
	# conta quantas vezes cada palavra aparece e dps adiciona ao dicionario
	for palavra in lista_palavras:
		qtda = lista_palavras.count(palavra)
		if palavra in dicionario:
			continue
		else:
			dicionario[palavra] = qtda
	# percorre o dicionario
	for chave, dados in dicionario.items():
		print("A palavra |%s| é citada %d vezes." % (chave.center(15), dados))

	arquivo_texto.close()
	return 0

if __name__ == '__main__':
	main()
