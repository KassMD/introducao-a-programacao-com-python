#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Crie um programa que receba uma lista de nomes de arquivo e que gere apenas um
grande arquivo de saída.
'''
def main():
	file_list = []
	output_file = open("arquivo_saida.txt", "w")
	while True:
		file_name = input("Digite o nome do arquivo, com extensão (ou S para sair): ")
		if file_name == "S" or file_name == "s":
			break
		else:
			file_list.append(file_name)
			
	for name in file_list:
		try:
			file_open = open(name, "r")
			output_file.write("\n%s:\n" % name)
			for line in file_open.readlines():
				output_file.write("\t%s" % line)
			print("%s foi salvo!" % name, end="")
			file_open.close()
		except FileNotFoundError:
			print("!%s não foi encontrado!" % name, end="")
		print("\n")

	output_file.close()
	return 0

if __name__ == '__main__':
	main()
