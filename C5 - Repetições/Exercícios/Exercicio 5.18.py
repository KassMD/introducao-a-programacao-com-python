# -*- coding: utf-8 -*-
''' Modifique o programa anterior (Listagem 5.14) para trabalhar
com notas de R$100. '''

valor = int(input("Digite o valor a pagar: "))
cedulas = 0
atual = 100
apagar = valor
while True:
    if atual <= apagar:
        apagar = apagar - atual
        cedulas = cedulas + 1
    else:
        print("%d cédula(s) de %d" % (cedulas, atual))
        if apagar == 0:
            break
        elif atual == 100:
            atual = 50
        elif atual == 50:
            atual = 20
        elif atual == 20:
            atual  = 10
        elif atual == 10:
            atual = 5
        elif atual == 5:
            atual = 1
        cedulas = 0
