#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.12
	Escreva uma função que receba uma string e uma lista. A função deve comparar a string 
	passada com os elementos da lista, também passado como parâmetro. Retorne verdadeiro 
	se a string for encontrada dentro da lista, e falso em caso contrário.
'''
def main():
	frase = RFrase()
	lista = RLista()
	print("\tString: ", frase, "\n\tLista: ", lista)
	print(Compara(frase, lista))
	return 0

def RFrase():
	frase = input("Digite uma frase: ")
	return frase
	
def RLista():
	lista = []
	while True:
		indice = input("Digite uma frase[ou 0 para sair]: ")
		if indice == '0':
			break 
		lista.append(indice)
	return lista
	
def Compara(frase, lista):
	if frase in lista:
		return("Frase encontrada dentro de lista.")
	else:
		return("Frase não existe dentro de lista.")

if __name__ == '__main__':
	main()
