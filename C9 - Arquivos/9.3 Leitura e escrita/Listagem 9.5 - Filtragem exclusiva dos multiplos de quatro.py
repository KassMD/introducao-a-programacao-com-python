#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Listagem 9.5 - Filtragem exclusiva dos multiplos de quatro
def main(args):
	multiplos4 = open("multiplos de 4.txt", "w")
	pares = open("pares.txt")
	for linha in pares.readlines():
		if int(linha) % 4 == 0:
			multiplos4.write(linha)
	pares.close()
	multiplos4.close()
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
