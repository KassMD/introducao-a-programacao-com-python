#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 8.3
	Escreva uma função que receba o lado(L) de um quadrado e retorne sua área ( A = lado²)
	area_quadrado(4) == 16
	area_quadrado(9) == 81
'''
def main():
	lado = int(input("Digite o lado do quadrado: "))
	print("A área do quadrado é", AreaQuadrado(lado))
	
	return 0

def AreaQuadrado(Lado):
	Area = Lado ** 2
	return Area
	
if __name__ == '__main__':
	main()
