# -*- coding: utf-8 -*-
''' Escreva um programa que calcule o resto da divisão inteira entre dois números. Utilize apenas
as operações de soma e subtração para calcular o resultado. '''

dividendo = int(input("Digite um dividendo: "))
divisor = int(input("Digite o divisor: "))
resto = dividendo
while resto >= divisor :
    resto -= divisor
print("%d / %d = %d\nresto = %d" % (dividendo, divisor, (dividendo / divisor), resto))
