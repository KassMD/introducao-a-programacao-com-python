#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Listagem 8.4 - Reutilização da função epar em outra função
'''
	Podemos entender o 'return' como uma interrupção da execução da função, seguido 
	do retorno do valor. As linhas da função após a instrução 'return' são ignoradas de 
	forma similar à instrução 'break' dentro de um 'while' ou 'for'.
'''
def main():
	while True:
		valor = int(input("Insira um valor [0 para sair]: "))
		if valor == 0: break
		print(epar(valor))
		
	return 0

def epar(x):
	if (x%2 == 0):
		return "Par"
	else:
		return "Ímpar"

if __name__ == '__main__':
	main()
