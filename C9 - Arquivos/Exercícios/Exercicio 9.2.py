#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' Exercicio 9.2
	Modifique o programa do exercício 9.1 para que receba mais dois parâmetros: a linha de 
	início e a de fim para impressão. O programa deve imprimir apenas as linhas entre esses 
	dois valores (incluindo as linhas de início e fim).
'''
def main():
	nomearq = input("Insira o nome do arquivo(com extensão): ")
	linhainicio = int(input("Insira a linha de inicio: "))
	linhafim = int(input("Insira a linha final: "))
	try:
		arquivo = open(nomearq, "r")
		for linha, conteudo in enumerate(arquivo.readlines()):
			if linha >= linhainicio and linha <= linhafim:
				print(conteudo, end="")
		
	except FileNotFoundError:
		print(" ERRO! Arquivo não encontrado.")
	return 0
	
if __name__ == '__main__':
	main()
