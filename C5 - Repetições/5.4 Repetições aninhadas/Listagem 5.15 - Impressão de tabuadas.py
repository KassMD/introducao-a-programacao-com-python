# -*- coding: utf-8 -*-
# Listagem 5.15 - Impressão de tabuadas
# Tabuada de 1 a 10

tabuada = int(input("Digite a tabuada: "))

while tabuada <= 10:
	numero = 1
	while numero <= 10:
		print("%d x %d = %d" % (tabuada, numero, tabuada * numero))
		numero += 1
	tabuada += 1
	print("\n")
