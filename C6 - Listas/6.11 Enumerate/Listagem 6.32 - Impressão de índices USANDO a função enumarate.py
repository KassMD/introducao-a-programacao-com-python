# -*- coding: utf-8
# Listagem 6.32 - Impressão de índices USANDO a função enumarate

def main():
	L = [5,9,13]
	for x, e in enumerate(L):
		print("[%d] = %d" % (x,e))
		
	return 0

if __name__ == '__main__':
	main()

''' A função enumerate gera uma tupla em que o primeiro valor é o índice 
e o segundo é o elemento da lista sendo enumerada. Ao utilizarmos 'x', 'e' 
em for, indicamos que o primeiro valor da dupla deve ser colocado e 'x', 
e o segundo valor em 'e'.
Assim na primeira iteração teremos a tupla (0,5), onde x=0 e e=5. 
Isso é possivél porque Python permite o desempacotamento de valores de uma Tupla, 
atribuindo um elemento da Tupla a cada variável em for. '''
