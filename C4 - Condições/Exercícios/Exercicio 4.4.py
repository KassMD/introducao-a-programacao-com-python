# -*- coding: utf-8 -*-
''' Escreva um programa que pergunte o salário do funcionário e calcule o 
valor do aumento. Para salários superiores a R$1.250,00, calcule um aumento 
de 10%. Para inferiores ou iguais, de 15%. '''

salario = float(input("Digite o salário: "))
if salario > 1.250:
	aumento = salario * 0.10
if salario < 1.250 or salario == 1.250:
	aumento = salario * 0.15
	
print "Salário novo: %5.3f" % (salario + aumento)
