#!/usr/bin/python3
# -*- coding: utf-8 -*-
''' Escreva um programa para controlar uma pequena máquina registradora. Você
deve solicitar ao usuário que digite o código do produto e a quantidade comprada.
Utilize a tabela de códigos abaixo para obter o preço de cada produto:
Código 1|Preço 0,50
Código 2|Preço 1,00
Código 3|Preço 4,00
Código 5|Preço 7,00
Código 9|Preço 8,00
Seu programa deve exibir o total das compras depois que o usuário digitar 0.
Qualquer outro código deve gerar a mensagem de erro "Código inválido.''' 

print ("\t\tMaquina registradora")
total= 0
while True:
    cod_produto = int(input("Insira o código do produto: "))
    qtda_comprada = int(input("Insira a quantidade: "))

    if cod_produto == 1:
        total = total + (0.50 * qtda_comprada)
    elif cod_produto == 2:
        total = total + (1.00 * qtda_comprada)
    elif cod_produto == 3:
        total = total + (4.00 * qtda_comprada)
    elif cod_produto == 5:
        total = total + (7.00 * qtda_comprada)
    elif cod_produto == 9:
        total = total + (8.00 * qtda_comprada)

    elif cod_produto == 0:
        print("Valor total: %3.2f" % total)
        break
    else:
        print("CÓDIGO INVÁLIDO!")
        break
