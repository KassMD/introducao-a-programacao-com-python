# -*- coding: utf-8 -*-
''' Faça um programa que leia duas listas e que gere uma terceira com os elementos das duas
primeiras. '''
def main():
    array_a = []
    array_b = []
    cont = 0

    print("\t\tArray 1")
    while True:
        cont = int(input("Digite um valor (ou 0 para sair): "))
        if cont == 0:
            break
        array_a.append(cont)

    cont = 0
    print("\t\tArray 2")
    while True:
        cont = int(input("Digite um valor (ou 0 para sair): "))
        if cont == 0:
            break
        array_b.append(cont)

    array_c = [] # Cria a lista
    array_c.extend(array_a) # Adiciona a lista 'a' a lista 'c'
    array_c.extend(array_b) # Adiciona a lista 'b' a lista 'c'
    print(array_c)
    
    return 0
    
if __name__ == '__main__':
    main()
