# -*- coding: utf-8 -*-
''' Escreva um programa que leia um valor em metros e o exiba convertido em milímetros
'''

metro = int(input("Metros: "))
centimetros = metro * 100
mili = centimetros * 10
print("Convertido em milímetros dá %d" % mili)
